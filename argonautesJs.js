const url = "http://localhost:5000/names";

const argonautes = []
// Définition du nombre de nombre de colonnes
var nombreDeColonnes = 3
// Définition de ma limite d'objet dans un tableau'
var maxItems = 50

// Lancement de ma fonction quand toute ma page est chargée
document.addEventListener('focus', getNames());

// function qui me permet de mettre les données dans mon tableau
function dataPush(data) {
  // Je parcours ma data et je la met dans mon tableau "argonautes"
  for (let d = 0; d < data.length; d++) {
    argonautes.push(data[d].name)
  }
  test()
}

// "ColonneMax" Pour avoir le nombre de colonnes restantes sur la dernière ligne

function test() {
  // ligneMax compte tout les lignes, permet d'arrondir à la valeur supérieure
  // let ligneMax = Math.ceil(argonautes.length / nombreDeColonnes)
  // Défini si j'ai des données dans ma base de donnée
  if (argonautes != null || argonautes.length <= 0) {
    // Si le nombre d'item dans ma base de donnée est supérieur à la limite que j'ai fixé
    // 
    if (argonautes.length > maxItems) {
      // ligneMax compte tout les lignes, permet d'arrondir à la valeur supérieure
      var ligneMax = Math.ceil(maxItems / nombreDeColonnes)
      var colonnesMax = maxItems - (parseInt(maxItems / nombreDeColonnes) * nombreDeColonnes)
    } else {
      // ligneMax compte tout les lignes, permet d'arrondir à la valeur supérieure
      var ligneMax = Math.ceil(argonautes.length / nombreDeColonnes)
      var colonnesMax = argonautes.length - (parseInt(argonautes.length / nombreDeColonnes) * nombreDeColonnes)
    }
  } else {
    return (console.log("Aucune donnée"))
  }
  if (colonnesMax == 0) {
    ligneMax++
  }
  // Je lance ma fonction avec deux paramètres
  CreateTable(colonnesMax, ligneMax)
}
// 2 colonnes et 15 lignes
function CreateTable(colonnesMax, ligneMax) {
  // Je crée mon tableau afin de le remplir de ma data
    const table = document.createElement("table")
    // Tu boucles ligne après ligne
  for (let index = 0; index < ligneMax; index++) {
    // Si tu n'es pas dans la dernière ligne
    if (index != ligneMax - 1) { 
      // Récolte :
      // "index" =  le nombre d'itération de ligne
      // "nommbreDeColonnes" = nombre de colonnes
      // "table" = le rendu de mon tableau
      innerMyHTLM(index, nombreDeColonnes, table)
    } else {
    // Dernière ligne du tablau  = cette fois si je récolte le même index et table
    // "colonnesMax" = nombre de colonnes restantes
      innerMyHTLM(index, colonnesMax, table)
    }
  }
}

// let footerHtml = document.getElementsByClassName("member-list")
// let tableMember = document.getElementById("member50")
// console.log(footerHtml)
// Fonction qui me permet de crée une ligne dans mon tableau
function innerMyHTLM(index, y, table) {
  // Création de ma ligne
  const tr = document.createElement("tr")
  for (let x = 0; x < y; x++) {
    // Création de ma celule dans ma ligne
    const td = document.createElement("td")
    // J'ajoute le contenu de ma data dans la colone
    td.textContent = argonautes[index *nombreDeColonnes+x]
    // place ma celule dans ma ligne
    tr.appendChild(td);
    // place ma ligne dans mon tableau
    table.appendChild(tr);
    // met le tout dans le HTML
    document.getElementById("member50").appendChild(table);
  }
}

// Ajax qui me permet de faire une requette GET
function getNames() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    // function execute after request is successful 
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            // console.log(JSON.parse(this.responseText).Content );
            dataPush(JSON.parse(this.responseText).Content)
        }
    }
    // Sending our request 
    xhr.send();
}

// https://code.tutsplus.com/articles/create-a-javascript-ajax-post-request-with-and-without-jquery--cms-39195
function postName() {
  let newName = {
    name: document.getElementsByClassName("buttonB")[0].value
  }
    let post = JSON.stringify(newName);
    console.log(post)
    let xhr = new XMLHttpRequest()

    xhr.open('POST', url, true)
    xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
    xhr.send(post);

    xhr.onload = function () {
        if (xhr.status === 201) {
            console.log("Post successfully created!")
        }
    }
}